import m from 'mithril';
import velocity from 'velocity-animate';
import { cOne, cTwo, cThree, cFour, cTitle } from './components';

const logo = {};


logo.properties = {
    'x': '0px',
    'y': '0px',
    'width': '100%',
    'height': '100%',
    'viewBox': '0 0 224 230',
    'enable-background': 'new 0 0 224 230',
    'xml:space': 'preserve',
    config(element, isInitialized) {
        if (!isInitialized) {
            logo.isotype = element.getElementById('isotype');
            logo.title = element.getElementById('c-title');
            logo.title.style.display = 'none';
        }
    }
};

logo.show = function () {
    const paths = logo.isotype.getElementsByTagName('path');
    let element;
    for (let i = 0, len = paths.length; i < len; i++) {
        element = paths[i];
        if ( !element.dataset || !element.dataset.fill ) {
            // weird FF,old browsers fix
            element.dataset = {};
            element.dataset.fill = element.attributes.getNamedItem( 'data-fill' ).value;

        }
        velocity(element, {
            fill: element.dataset.fill,
            stroke: element.dataset.fill
        });
    }
    velocity(logo.title, 'fadeIn', { duration: 2000 });
};

const drawBarrier = {
    cantDraws: 0,
    finish() {
        this.cantDraws--;
        if ( this.cantDraws === 0 ) {
            logo.show();
            return;
        }
    }
};

logo.cArgs = function () {
    return {
        drawBarrier
    };
};

logo.view = function () {
    return m('svg#logo[type="image/svg+xml"]', logo.properties, [
        m('g#isotype', [
            m.component(cOne, logo.cArgs()),
            m.component(cTwo, logo.cArgs()),
            m.component(cThree, logo.cArgs()),
            m.component(cFour, logo.cArgs())
        ]),
        m.component(cTitle),
        m('image.logo-fallback[xlink:href=""]', { src: 'img/logo.jpg', alt: 'GEUT logo' })
    ]);
};

export default logo;
