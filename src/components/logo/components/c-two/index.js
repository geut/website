import m from 'mithril';
import common from '../common';
import data  from './data.json';

const cTwo = {
    options: {
        config(element, isInit) {
            if (!isInit) {
                cTwo.element = element;

                cTwo.drawBarrier.cantDraws++;
                common.drawGroup(element, () => {
                    // On complete
                    cTwo.drawBarrier.finish();
                });
            }
        }
    },

    view(ctrl, args = {}) {
        cTwo.drawBarrier = args.drawBarrier;
        args.attr = args.attr || {};
        return m('g#c-two.circle', Object.assign(args.attr, cTwo.options), [
            m('path.main', data.main),
            m('path.multiply', data.multiply)
        ]);
    }
};

export default cTwo;
