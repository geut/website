import m from 'mithril';
import common from '../common';
import data  from './data.json';

const cOne = {
    options: {
        config(element, isInit) {
            if (!isInit) {
                cOne.element = element;

                cOne.drawBarrier.cantDraws++;
                common.drawGroup(element, () => {
                    // On complete
                    cOne.drawBarrier.finish();
                });
            }
        }
    },

    view(ctrl, args = {}) {
        cOne.drawBarrier = args.drawBarrier;
        args.attr = args.attr || {};
        return m('g#c-one.circle', Object.assign(args.attr, cOne.options), [
            m('path.main', data.main)
        ]);
    }
};

export default cOne;
