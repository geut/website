import m from 'mithril';
import common from '../common';
import data  from './data.json';

const cThree = {
    options: {
        config(element, isInit) {
            if (!isInit) {
                cThree.element = element;

                cThree.drawBarrier.cantDraws++;
                common.drawGroup(element, () => {
                    // On complete
                    cThree.drawBarrier.finish();
                });
            }
        }
    },

    view(ctrl, args = {}) {
        cThree.drawBarrier = args.drawBarrier;
        args.attr = args.attr || {};
        return m('g#c-three.circle', Object.assign(args.attr, cThree.options), [
            m('path.main', data.main),
            m('path.multiply', data.multiply)
        ]);
    }
};

export default cThree;
