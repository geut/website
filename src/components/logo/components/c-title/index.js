import m from 'mithril';
import data  from './data.json';

const cTitle = {
    fill: '#4F5186',

    view() {
        return m('g#c-title', [
            m('path#letter-g', {
                fill: cTitle.fill,
                d: data.g
            }),
            m('path#letter-e', {
                fill: cTitle.fill,
                d: data.e
            }),
            m('path#letter-u', {
                fill: cTitle.fill,
                d: data.u
            }),
            m('path#letter-t', {
                fill: cTitle.fill,
                d: data.t
            })
        ]);
    }
};

export default cTitle;
