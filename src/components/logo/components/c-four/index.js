import m from 'mithril';
import common from '../common';
import data  from './data.json';

const cFour = {
    options: {
        config(element, isInit) {
            if (!isInit) {
                cFour.element = element;

                cFour.drawBarrier.cantDraws++;
                common.drawGroup(element, () => {
                    // On complete
                    cFour.drawBarrier.finish();
                });
            }
        }
    },

    view(ctrl, args = {}) {
        cFour.drawBarrier = args.drawBarrier;
        args.attr = args.attr || {};
        return m('g#c-four.circle', Object.assign(args.attr, cFour.options), [
            m('path.main', data.main),
            m('path.multiply', data.multiplyOne),
            m('path.multiply', data.multiplyTwo)
        ]);
    }
};

export default cFour;
