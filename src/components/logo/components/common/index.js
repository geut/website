import velocity from 'velocity-animate';

const common = {
    draw(element, cb) {

        element.style.strokeDasharray = element.getTotalLength();
        element.style.strokeDashoffset = element.getTotalLength();
        if ( !element.dataset ) {
            // weird FF,old browsers fix
            element.dataset = {};
            element.dataset.start = element.attributes.getNamedItem( 'data-start' ).value;
            element.dataset.duration = element.attributes.getNamedItem( 'data-duration' ).value;
        }
        velocity(element, {
            strokeDashoffset: 0
        }, {
            easing: 'easeInOutQuad',
            delay: parseInt(element.dataset.start),
            duration: parseInt(element.dataset.duration),
            complete: cb
        });
    },

    drawGroup(element, cb) {
        const paths = element.getElementsByTagName('path');
        for (let i = 0, len = paths.length; i < len; i++) {
            if (paths[i].classList.contains('main')) {
                common.draw(paths[i], cb);
            } else {
                common.draw(paths[i]);
            }
        }
    }
};

export default common;
