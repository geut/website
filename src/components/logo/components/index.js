export { default as cOne } from './c-one';
export { default as cTwo } from './c-two';
export { default as cThree } from './c-three';
export { default as cFour } from './c-four';
export { default as cTitle } from './c-title';
