import m from 'mithril';
import logo from './components/logo';

const app = {};

// controller
app.controller = function () {
    const that = {};

    return that;
};

app.animateIn = function (from) {
    return function (element, isInitialized) {
        if (!isInitialized) {
            app.transform(from, element);
        }
    };
};

// view
app.view = function () {
    return m('a[href="mailto:contact@geutstudio.com"][style="width: 90%;height: 70%;"]', [
        m.component(logo)
    ]);
};

// initialize
m.module(document.getElementById('app'), app);
