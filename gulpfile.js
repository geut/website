const lost = require('lost');
const appfy = require('gulp-appfy-tasks');
appfy.init(__dirname, {
    destPath: 'public/dist',
    browsersync: {
        server: {
            baseDir: 'public'
        }
    },
    assetsTemplate: 'assets/[name].[ext]',
    postcss: {
        plugins(config, plugins) {
            plugins.lost = lost();
            return plugins;
        }
    }
});
appfy.defineTasks();
