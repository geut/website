# website
> Geut Studio Website

## Getting Started

Install npm dependencies:

```bash
$ cd projectDir
$ npm install
```

### Usage

Start server:

```bash
$ gulp serve
```

Clean project:

```bash
$ gulp clean
```

Build project:

```bash
$ gulp build
```

```bash
# put your shell code here
pm2 stop geutstudio.com
rsync -avzhr --progress --delete --exclude '/public/dist' --exclude '/node_modules' --exclude '/.git' . /var/www/geutstudio.com
cd /var/www/geutstudio.com
npm install
gulp build
pm2 restart geutstudio.com
```

### Author
[geut]

### Version
1.0.0

[geut]:geut.studio@gmail.com
