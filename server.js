var Hapi = require('hapi'),
    path = require('path');

// Create a server with a host and port
var server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port: 3010,
    routes: {
        files: {
            relativeTo: __dirname
        }
    }
});

// event on start
server.start(function () {
    console.log('Server running at:', server.info.uri);
});

// Add the route
server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: 'public',
            index: true
        }
    }
});

// Start the server
server.start();
